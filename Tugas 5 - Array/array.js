console.log('=== SOAL 1 ===')

var angka = []; // variabel penampung nilai array

function range(startNum, finishNum) {
    angka = []; // initial state nilai array kosong
    if (startNum < finishNum) { // kondisi ascending jika startNum lebih kecil dari finishNum
        while (startNum <= finishNum) {
            angka.push(startNum);
            startNum++;
        }
        return angka;
    } else if (startNum > finishNum) { // kondisi descending jika startnum lebih besar dari finishNum
        while (finishNum <= startNum) {
            angka.push(startNum);
            startNum--;
        }
        return angka;
    } else {
        return (-1);
    }

}

console.log(range(1, 10));
console.log(range(1));
console.log(range(11, 18));
console.log(range(54, 50));
console.log(range());

console.log('=== END SOAL 1 === \n')

console.log('=== SOAL 2 ===')
function rangeWIthStep(startNum, finishNum, step) {
    angka2 = [];
    if (startNum < finishNum) { // kondisi ascending jika startNum lebih kecil dari finishNum
        while (startNum <= finishNum) {
            angka2.push(startNum);
            startNum = startNum + step;
        }
        return angka2;
    } else if (startNum > finishNum) { // kondisi descending jika startnum lebih besar dari finishNum
        while (finishNum <= startNum) {
            angka2.push(startNum);
            startNum = startNum - step;
        }
        return angka2;
    } else {
        return (-1);
    }
}

console.log(rangeWIthStep(1, 10, 2));
console.log(rangeWIthStep(11, 23, 3));
console.log(rangeWIthStep(5, 2, 1));
console.log(rangeWIthStep(29, 2, 4));

console.log('=== END SOAL 2 === \n')

console.log('=== SOAL 3 ===')

function sum(startNum, finishNum, step) {
    angka3 = []; // initialnya array di set kosong
    var i = 0; // variabel index array
    var hasiljumlah = 0; // hasil penjumlahan awal diset kosong
    if (startNum < finishNum) { // kondisi ascending jika startNum lebih kecil dari finishNum
        while (startNum <= finishNum) {
            angka3.push(startNum);
            if (step == undefined) {
                startNum++;
            } else {
                startNum = startNum + step;
            }
            hasiljumlah = hasiljumlah + angka3[i];
            i++;
        }
        return hasiljumlah;
    } else if (startNum > finishNum) { // kondisi descending jika startnum lebih besar dari finishNum
        while (finishNum <= startNum) {
            angka3.push(startNum);
            if (step == undefined) {
                startNum--;
            } else {
                startNum = startNum - step;
            }
            hasiljumlah = hasiljumlah + angka3[i];
            i++;
        }
        return hasiljumlah;
    } else if (startNum == undefined) {
        return 0;
    } else {
        return startNum;
    }

}

console.log(sum(1, 10))
console.log(sum(5, 50, 2))
console.log(sum(15, 10))
console.log(sum(20, 10, 2))
console.log(sum(1))
console.log(sum())

console.log('=== END SOAL 3 === \n')

console.log('=== SOAL 4 ===')

function dataHandling(input) {
    i = 0;
    idnya = [];
    while (i < input.length) {
        id = input[i][0];
        namanya = input[i][1];
        alamat = input[i][2];
        tanggal = input[i][3];
        hobi = input[i][4];
        idnya.push('Nomor ID: ' + id + ' Nama Lengkap: ' + namanya + ' TTL: ' + alamat + ' ' + tanggal + '  Hobbi: ' + hobi);
        i++;
    } return [idnya];
}

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

console.log(dataHandling(input))
console.log('=== END SOAL 4 === \n')

console.log('=== SOAL 5 ===')

// Code di sini
function balikKata(str) {
    var currentString = str;
    var newString = '';
    for (let i = str.length - 1; i >= 0; i--) {
        newString = newString + currentString[i];
    }

    return newString;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

console.log('=== END SOAL 5 === \n')

console.log('=== SOAL 6 ===')
function dataHandling2(input, inputKedua) {
    input[1] = inputKedua[1];
    input[2] = inputKedua[2];
    input[4] = inputKedua[4];
    input[5] = inputKedua[5];
    hasil = input[3].split('/');
    hasilnya = hasil[1];
    switch (hasilnya) {
        case '01': { hasilnya = 'Januari'; break; }
        case '02': { hasilnya = 'Februari'; break; }
        case '03': { hasilnya = 'Maret'; break; }
        case '04': { hasilnya = 'April'; break; }
        case '05': { hasilnya = 'May'; break; }
        case '06': { hasilnya = 'Juni'; break; }
        case '07': { hasilnya = 'Juli'; break; }
        case '08': { hasilnya = 'Agustus'; break; }
        case '09': { hasilnya = 'September'; break; }
        case '10': { hasilnya = 'Oktober'; break; }
        case '11': { hasilnya = 'November'; break; }
        case '12': { hasilnya = 'Desember'; break; }

        default: {
            console.log('Jangan lebih dari 12'); break;
        }
    }

    return [input, hasilnya];
}
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
var inputKedua = ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"];
console.log(dataHandling2(input, inputKedua))

/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */