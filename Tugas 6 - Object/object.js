console.log('=== SOAL 1 ===')

var now = new Date()
var thisYear = now.getFullYear()

function arrayToObject(arr) {
    // Code di sini 
    if (arr.length == 0) {
        return '';
    } else {
        i = 0;
        while (i < arr.length) {
            var objek = arr[i][0].concat(' ', arr[i][1], ':')
            objek += '{\n '.concat('firstName: ', arr[i][0])
            objek += '\n '.concat('lastName: ', arr[i][1])
            objek += '\n '.concat('gender: ', arr[i][2])
            objek += '\n '.concat('age: ', thisYear - arr[i][3], '\n }')
            i++;
            return console.log(objek);

        }
    }
}

// Driver Code
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case 
arrayToObject([]) // ""

console.log('=== END SOAL 1 \n')

function shoppingTime(memberId, money) {
    // you can only write your code here!
    var listHarga = [1500000, 500000, 250000, 175000, 50000];
    var listProduct = ['Stacattu', 'Zoro', 'H&N', 'Uniklooh', 'Casing'];
    var dibeli = [];
    var sisa = money;
    i = 0;
    if (memberId == undefined || memberId == '') {
        return console.log('Mohon maaf toko X hanya berlaku untuk member saja')
    } else if (money < 50000) {
        return console.log('Mohon maaf, uang tidak cukup')
    } else {
        while (i < listHarga.length) {
            if (money > listHarga[i]) {
                dibeli.push(listProduct[i]);
                sisa = sisa - listHarga[i];
            }
            i++;
        }
    }
    return console.log('Money: ' + money + '\nListPurchased: ' + dibeli + '\nSisa: ' + sisa)
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000))
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000))
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
console.log('=== END SOAL 2 === \n')

console.log('=== SOAL 3 ===')

function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
    biaya = 0;
    x = 0;
    if (arrPenumpang.length == 0) {
        return '';
    } else {
        i = 0;
        while (i < arrPenumpang.length) {
            var objekPenumpang = {
                penumpang: arrPenumpang[i][0],
                asal: arrPenumpang[i][1],
                tujuan: arrPenumpang[i][2]
            }

            i++;
        }
        return console.log(objekPenumpang)
    }
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]