// Soal 1
console.log('<== Soal 1 - Game werewolf ==>')

var nama = 'Saya'; // Nama bisa diubah
var peran = 'werewolf'; // Peran bisa diubah ke guard, werewolf atau penyihir

if (nama == '') { // Kondisi nama kosong
    console.log('Nama harus diisi!')
} else if (nama != '' && peran == '') { //kondisi nama berisi namun peran kosong
    console.log('Hallo ' + nama + ', pilih peranmu untuk memulai game')
} else if (nama != '' && peran == 'werewolf') { // Kondisi peran werewolf
    console.log('Selamat datang di dunia werewolf, ' + nama)
    console.log('Haloo werewolf ' + nama + ', kamu akan memangsa setiap malamnya')
} else if (nama != '' && peran == 'penyihir') { // Kondisi peran penyihir
    console.log('Selamat datang di dunia werewolf, ' + nama)
    console.log('Haloo penyihir ' + nama + ', kamu dapat melihat siapa yang menjadi werewolf')
} else if (nama != '' && peran == 'guard') { // Kondisi peran guard
    console.log('Selamat datang di dunia werewolf, ' + nama)
    console.log('Haloo guard ' + nama + ', kamu akan membantu melindungi temanmu dari serangan werewolf')
}

// SOal 2
console.log('<== Soal 2 Switch ==>')

var hari = 2;
var bulan = 12;
var tahun = 1955;

switch (bulan) {
    case 1: { bulan = 'Januari'; break; }
    case 2: { bulan = 'Februari'; break; }
    case 3: { bulan = 'Maret'; break; }
    case 4: { bulan = 'April'; break; }
    case 5: { bulan = 'May'; break; }
    case 6: { bulan = 'Juni'; break; }
    case 7: { bulan = 'Juli'; break; }
    case 8: { bulan = 'Agustus'; break; }
    case 9: { bulan = 'September'; break; }
    case 10: { bulan = 'Oktober'; break; }
    case 11: { bulan = 'November'; break; }
    case 12: { bulan = 'Desember'; break; }

    default: {
        console.log('Jangan lebih dari 12'); break;
    }
}

console.log(hari + ' ' + bulan + ' ' + tahun)