console.log('=== 1 Animal Class ===')
console.log('= Release 0 =')

class Animal {
    // Code class di sini
    constructor(brand) {
        this.nama = brand;
        this.kaki = 4;
        this.cold_blooded = false;
    }
    get name() {
        return this.nama;
    }
    get legs() {
        return 4;
    }
}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log('\n= Release 1 =')
// Code class Ape dan class Frog di sini
class Ape extends Animal {
    constructor(brand) {
        super(brand);
    }
    yell() {
        return 'Auoo';
    }
}
class Frog extends Animal {
    constructor(brand) {
        super(brand);
    }
    jump() {
        return 'hop hop';
    }
}

var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

console.log('=== END Soal 1 === \n')

console.log('=== 2 Function to Class ===')
class Clock {
    constructor({ template }) {
        this._template = template;
    }

    render() {
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        var output = this._template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);

        console.log(output);
    }

    stop() {
        clearInterval(this._timer);
    };

    start() {
        this.render();
        this._timer = setInterval(this.render.bind(this), 1000);
    };

}

var clock = new Clock({ template: 'h:m:s' });
clock.start(); 