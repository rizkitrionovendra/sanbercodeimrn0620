console.log('=== SOAL 1 - While ===')
console.log('== LOOPING PERTAMA ==')

var flag = 1; // variabel untuk looping
var number = 2; // variabel nilai awal penomoran

while (flag <= 10) { //untuk looping 10 kali

    console.log(number + '. I love coding')
    flag++;
    number = number + 2; // untuk penomoran genap ascending

}

console.log('== LOOPING KEDUA ==')

var flag2 = 1; // variabel untuk looping
var number2 = 20; // variabel nilai awal penomoran

while (flag2 <= 10) { // untuk looping 10 kali

    console.log(number2 + '. I will become a mobile developer')
    flag2++;
    number2 = number2 - 2; // untuk penomoran genap descending

}

console.log('=== END SOAL 1 ===')

console.log('=== SOAL 2 - For ===')

for (var angka2 = 1; angka2 <= 20; angka2++) {

    if (angka2 % 3 == 0 && angka2 % 2 == 1) {
        console.log(angka2 + ' - I love coding');
    } else if (angka2 % 2 == 0) {
        console.log(angka2 + ' - Berkualitas');
    } else {
        console.log(angka2 + ' - Santai');
    }

}

console.log('=== END SOAL 2 ===')

console.log('=== SOAL 3 - Persegi Panjang ===')

for (var angka3 = 1; angka3 <= 4; angka3++) {
    console.log('########')
}

console.log('=== END SOAL 3 ===')

console.log('=== SOAL 4 - Membuat tangga ===')

var pagar = '#'; // variabel untuk pagar

for (var angka4 = 1; angka4 <= 7; angka4++) {
    console.log(pagar)
    pagar = pagar + '#' // looping pagar bertambah
}

console.log('=== END SOAL 4 ===')

console.log('=== SOAL 5 - Papan catur ===')

var flag2 = 1; // variabel untuk looping

while (flag2 <= 8) { //untuk looping 8 kali

    if (flag2 % 2 == 1) {
        console.log(' # # # #') // papan catur saat ganjil
    } else {
        console.log('# # # # ') // papan catur saat genap
    }
    flag2++;
}

console.log('=== END SOAL 5 ===')