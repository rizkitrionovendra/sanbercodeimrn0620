console.log('=== SOAL 1 - Arrow ===')

var golden = () => {
    console.log('This is golden !')
}

golden()

console.log('=== END SOAL 1 === \n')
console.log('=== SOAL 2 - Objek Literal ===')

const newFunction = function literal(firstName, lastName) {
    return {
        fullName: function () {
            console.log(firstName + " " + lastName)
        }
    }
}

newFunction("William", "Imoh").fullName()
console.log('=== END SOAL 2 === \n')
console.log('=== SOAL 3 Destructuring ===')

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwart REact Conf",
    occupation: "Dave-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation } = newObject;

console.log(firstName, lastName, destination, occupation)
console.log('=== END SOAL 3 === \n')
console.log('=== SOAL 4 - Array Spreading ===')
let west = ["Will", "Chris", "Sam", "Holly"]
let east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [west, east]

console.log(combined)
console.log('=== END SOAL 4 \n')
console.log('=== SOAL 5 - Template Literals ===')

const planet = "earth"
const view = "glass"
const before = 'Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enimad minim veniam'

console.log(before)